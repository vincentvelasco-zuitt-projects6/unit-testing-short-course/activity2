
const {checkAge, checkFullName} = require('../src/util.js');


const {assert} = require('chai');

/*
	Age:
			- check if it is a integer value
			- check if it is NOT empty
			- check if it is NOT undefined
			- check if it is NOT null
			- check if it is NOT equal to 0

*/

describe('test_checkAge', () => {

	it('age_is_integer', () => {
		const age = 12;
		// assert.isNumber(age, checkAge(age));
		// assert.strictEqual(typeof(checkAge(age)), 'number', "Error: Age should be a number");
		expect(checkAge(age)).to.be.a('number');
	})

	it('age_not_empty', () => {
		const age = '';
		assert.isNotEmpty(checkAge(age));
	})

	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age));
	})

	it('age_not_null', () => {
		const age = null;
		assert.isNotNull(checkAge(age));
	})

	it('age_not_0', () => {
		const age = 0;
		assert.notStrictEqual(checkAge(age));
	})



})




/*
Fullname:
		- check if it is NOT empty
		- check if it is NOT null
		- check if it is NOT undefined
		- check if it is a string data type
		- check if the character is NOT equal to 0

*/


describe('test_fullName', () => {

	it('fullName_not_empty', () => {
		const fullName = '';
		assert.isNotEmpty(checkFullName(fullName));
	})

	it('fullName_not_null', () => {
		const fullName = null;
		assert.isNotNull(checkFullName(fullName));
	})


	it('fullName_not_undefined', () => {
		const fullName = undefined;
		assert.isDefined(checkFullName(fullName));
	})

	it('fullName_is_string', () => {
		const fullName = 24;
		assert.isString(checkFullName(fullName));
	})


	it('fullNameLength_not_0', () => {
		const fullName = 0;
		assert.notStrictEqual(checkFullName(fullName));
	})

})