function checkAge(age){

	/*if(typeof age != 'a' ){

		return "Error: Age should be a number"

	}*/

	if(age == "" ){
		return "Error: Age is Empty!";
	}

	if(age === undefined){
		return "Error: Age should be defined!";
	}


	if(age === null){
		return "Error: Age should not be Null!";
	}

	if(age == 0){
		return "Error: Age should not be equal to 0!";
	}

	return age;
}



function checkFullName(fullName){

	if(fullName == "" ){
		return "Error: Full Name is Empty!";
	}

	if(fullName === null){
		return "Error: Full Name should not be Null!";
	}


	if(fullName === undefined){
		return "Error: Full Name should be defined!";
	}

	if(typeof(fullName) !== 'string'){
		return "Error: Email should be String!";
	}
	

	if(fullName.length === 0 ){
		return "Error: fullName character should be equal to 0!";
	}

	return fullName;
}




module.exports = {
	checkAge: checkAge,
	checkFullName: checkFullName
}